

# DONE IN CLASS

## Node Task
This project contains node
Hello-word

### Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal Commands

Code block:

```sh
git clone https://gitlab.com/IsabelFerriMolla/fork-repo
cd ./node-task
npm i
```
start cmd: `npm run start`

## App should work

![alt text for text image](https://baap.ponet.fi/share/mosquitto_broker.png)
