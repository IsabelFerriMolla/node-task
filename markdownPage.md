# ASSIGNMENT

Hi my name is **Isabel Ferri Mollá**, in the next markdown we are going to do a little sumary about **gitlab**

 

## *GITLAB PROJECT*

First of all we are going to explain how can we create a project in gitlab so  in order to do this we should:
1. Register our account in gitlab
2. Enter to your account
3. In the dashboard click the green green *new project* button
4. Choose the  type of project you want to create(normally a blank project)
5. select the project characteristics:
    - Give a name to the project
    - Decide if it will be public or private
    - Click on *create the project*

And that is all now we have our project ready

<br>

   

![alt text](https://cdn.dribbble.com/users/1480650/screenshots/4739771/autodevops-dribbble-gif.gif)

---
<br>

## *FIRST STEPS*

So now that we have a project we are going to explain how initialize git and how to use git remote in our computer


<br>

### **Git Initialization** 
The first thing we shoul do is configure the ***git CLI*** on our computer in order to do this we are gounf to add our **name** and our **email**:
``` sh
$ git config --global user.name "Isabel Ferri Mollá"
$ git config --global user.email "Isabel.Ferri.Molla@student.lab.fi"
```

To initialize git we must open the girbash command and then we should move to the directory where we want to create our repository<br>

> `cd C:/Users/Isabel/desktop/myGitProject `
> 
once there we will use the init command<br>
>`git init `

At this moment we it will appear in our folder a .git directory so git repositort is created  

Then we must add the remote address in order to pull and push things
```sh 
git remote add origin https://gitlab.com/IsabelFerriMolla/gitproject.git
git remote -v
```

![alt text](https://media.tenor.com/images/7192debcb144575e47b8581b97551239/tenor.gif)

---
<br>


## *BASIC COMANDS*

At this moment we already know:

- [x] How to create a Gitlab project
- [x] How to relate our machine with our repository
- [ ] What are the basic git commands in order to save the changes

So at this moment we are going to describe the basic git commands and what they do

| COMMAND   | DESCRIPTION | HOW TO USE IT|
| ----------- | ----------- |--------|
| Init| Turns a directory into an empty Git repository | `git init`|
| Status | Returns the current state of the repository. |`git status`|
|Clone|To create a local working copy of an existing remote repository|`git clone https://gitlab.com/IsabelFerriMolla/gitproject.git`| 
| Add | Adds files in the to the staging area for Git |`git add .`|
| Commit | Record the changes made to the files to a local repository |`git commit -m "message to the commit"`|
| Push | Sends local commits to the remote repository |`git push --set -upstream origin master` (depending on the branch)|
| Pull | To get the latest version of a repository |`git pull [options] [repository]"`|
|Branch|To determine what branch the local repository is on, add a new branch, or delete a branch.|`git branch -a`|
|Checkout|to switch branches.|`git checkout -b dev` (to create a new one)|
| Merge | Integrate branches together |`git merge dev`|


<br>


 [*Longer command explanation*](https://www.guru99.com/software-testing-introduction-importance.html#1)

<br>

 ![alt text](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXkAAACGCAMAAAAPbgp3AAABCFBMVEX////iQyn8bSb8oyaMkp2HjZnrXTqDipbhMw71yMOJj5ukqLHHys/19fa+wceaoKmVmqX8nQDQ09fb3eCtsLizt778aSThPB/8pibgPyjm5+n4+Pnv8PH8oR/8qC3hOBj8pTX+9vXqgnXrin7gLgD53NjnaVj8fjb8cCz+7tv/9OfwaD39uGLBxcrW2Nz8czLxr6f3z8rkTzfumI3lWkbvopj87+3pd2j9yYz8oDn8rEH/8N78jTv90Z38lTr8oTj+2K38gzf9vW/+1qn9w37+5sr+4L3kUjvmX0375eLocGDytK32zMb63dLqTSTtaEjxjnnzpI/8sU73cTr9u2nsXDH8n0z81L4aA3vlAAAOXUlEQVR4nO1daWObxhYVFRgHyRISWmwtXhKncdxITpzNdpt9a5q07704ee///5MHzAzMcmeGZbBryvlmgQZxuHPuubPgVkuGg4ePb0kPlsPdp08/V9R0HfBkc+/waSUt3z3c29u8W0nTdcCtTcuypgdVNP18GrZ8u4qWa4HbIT3WZhV6c2BFmFbQci1wsBfTU0Vkxr3J2vxaQdN1AKLH2qug6bg3NXIjA6KnksiMe1OISnLIjccBZmd6x3jTuDdZm4+MN10HPML0WJbxpu9MK3uodQChxzo0HZmkNzXuBsY0ocd0ZKa9yfhDrQNSeozLzZ30oT423HQdkNJjPBGmj9T6ZLblWuA0pWfPbGTSvakZuxFwl6LHOjXa9OO9yh5qHUDTYx0ajUyqN1VhWW86pjQ7RiPz7qFV2UOtAVh6jCZCpjdZe9WM/99csPQYTYSs2EyfmGu5FnjCqI3JyGRSd/RQm0lBGpzYmJQbrjc1csPiKUePdWgsMj9xLTfFFINTnh5jkfmZ702N3ND4vMnTM/3dUNNCb2rkhoZIj7HI/H3Ktzz92UzLtYBIj7X30EjLYm8ymUNuPEQtNua7H4q9ydRDrQMgeqzDZyaaFpyNZTCH3Hw8EcXGUGQ+A3pT2LSRh1oDQFpsKBGCvamRGwKYHiOR+TPUm6zpH+VbrgVgeqzNL6Vbfgb2JlM55MYD1mIjcvMF7k0mHmod8EUSmAYiE6gTTD3UOkAiNgYiUyY2IfWN3IT0yIi3ps9LNi3tTY3cRJDTU9rd/FHdQ60DnkvpKbt95Jkkv8ZNN+u5DyTOJo7Mcr5b0Zuq2RN0s3BLQU/J3WpysWnkpqUUm5KReaAQG6vZPnKgZKfUxiZlb2rkRk1PKbm5repNxnerbQXD3ng0Go1X64XZliuCmp4ykakRG2vT4F3Mer7nOo4XwXEcexSwx0d+CLub/N23o7/XQjtB9LnvFf8hW6iBrQynKpxNycjU9CaDWxC7fdezGXguRXOITnTcST/yo7/dodDS2o2+7Rb/KVtO3EAG5nX0lEiEmt5kTG7WtsPxHsOhQ5pnvh+fATDvXBnzOnqKbx850D1S69SIuxm5AO0x3FFyUlbmgytjXk9P4d1q+t5kYk/QwnZSiXEcNxT7tAe4Y3La34/5R1p6Cu+jvKPrTSa2IC5sQrPj2uPuOgiCdXdku+hxuHNyXid6Iu4q+V7fu27m9fQU3kepcTYRSr9gYU6Id+wVbSUXK99lYjxYR0hPuX7m9SFv7RWLzEca0xShtNx0CPE94dDQdnz5F6tmXltR/PnXhh7/+norP77++4e+5R//KX6LEVYouXr2DDg4H4l2PcG1M//+t8lPevy1mR/TDO3+dHJR/BZDLBxMfJa6hcW1M7+/fS8D9Zc7euHgsZGB+KNXg/vF77HVGiOt8QoMFiDmgRo2K/OLYD1cBzPhmTPML4LonLnw5Vbrw6C9f5QhNPMTv3Opb3Zy3l6e5eaMunmkNa5CVBIMexHSIYWYeW/US4A/z8T8YtWPhigieH6Pfe4p81s9G5/T7wrkny3b7fMMwbmRn/kTPfG/bbfbuxlYk6EX36TXyXJuJ6LApfy8hwsAwiD+PAPz69ChUjWz445pXhNvs/KSQsNzPN4BhLfe3n6p15v8cqMXm8m93fDqZeTGR3cOZVcBQiXFjzfgz7XMBz4/RBQ6WirsScx32Mra8ZmucX8Q3nt7942W+hNhL48u5PVic7IfXbyE3MxyhLwx5scC79Fv8NOo30LHhfY9+vHEYhNRf6KlPq/cnOrF5jy+9rK4u1nFJAH+BALMvJcCf65hvuMQ/YgHKgi91OPfggbvEPWUKF0g5tuvtFn2l5xyoxWbycttdO3Br/n45sikBgj0J/PMe51xAvy5hvnAjWn3+r1hEATDMRk0chPJS5n3ovzb9z3yfLx0/A6JTST157qgP8nHvFZsJm8w8e3li7yME6AbUtSpNEDmC7jKjue5o3X6tIcex2rCvNsfIn2ZjbHku4m1eoFDPqReW1Dlkxuds0HZtZzcIE/pjfVnIsYg5vNXUgt3xLrIGWaaPAzMvONTk2IzHPR98sFFwnx7V1dQ5ZMbndgc7SdXLiw3ccfPKvPGmG8JVRvytoncIOa5gCCVBz4pEZsIuoIqVzG184sm5M+pKw8Kyg2ePQr0Z0YwxryAOYoAIlyIeX6ssuvQz+MFzfy2rqDKIzcaZxOXUAmW33LcpnAzzADJiEfKbHXM40kW0jRmnusac5tOSpTYZCio8siNWmwmb3bpC7cHx3nuMwE2lfQt0i4xgptWjhUyP4qbIpMuMPN4iAn92uMBc/+6gipHGasRm5N99sKDd3nuMwEeO6C7NW+hnXQOqkLmY1KTCQIJ87Q2vuCY12XZ7HKjcTavuOsWlJued5OYRzkW/YBvS44BTUGVXW6UYpOUUBT1heQGxzyjNlfI/HwWDLvd4XA9G2VhvoU8cPSDeLGJoCyoMrsbpdiw2bWM3AA677kJvCqZX3Q7drzIIR4ARZfSMW8n5uYdwLyyoDrKPGqmEBuqhCorN0PWR8eMJNjqeJUxP+s44soqLfN+wrwoNm1Nls0qN6cK0TraB65azN2gnAUMAMRgJcAk8yNotDIH85DYtNUFVUZ3oxIbpoSimH+dhWoOM5cTFJafipif+2S+Aw9XOk425hO1eQ0zr5qhyig3itEySORjuXmQkW2GA5SzJMPzMfOeeebxuLvn+nhZ1XDVz8Q8cmLhWQ8gsWmrC6pscrMh7TbpAKVAfRG5wd0cPlgR8ysc4R0qvWRylS3iKo8lxCulPpPcKMTmBMiuWG4+6miWkcumWO6geebxokHme3n8/LD1USI2bVVBlU1u5M6GL6GomC8iN0NmFIpDNcyjS3K5JRPzAXFiZ9KYV2XZLHIjdTZACUUFvY5mAHidkw1OSo0rYR51M+6KmZjH1cdW672CefmwZQa5kYqNLLsWZx5nO2BJJaGD6g9mmEfWcMR+mIl59Ft9ejoKol5WUGWQG5mzAUuoFIWW3QzRfIMDLTGrhPm5DxnZLMwvPPLM7st1vq3Isnq5kYkNXEIlIV9sdgQb6z5wSMd8h/P7BGrm7aLM99KhyvdK6vclaVIrN1KxgUsojOW+lmQQQ+zxRuIhHfPouFgMZGCez+kZmJ+7lANWUy8ZttTKjURslNm1PdgvNjeS1DWOyKCO+a443hYjg87zyx06euY7TEb6uFRlWUlBpZMbWGzkJVRMfPFlZsTeeD4/HdvTMD+TVMBq5jGD9H7P+cixdcyP8Wpz8uHxN0XY78JZViM3O+DQ/ISfhaKxLFRFEQzJ6kW3w3C/6GiYxxOjglCpmV8JldQabxfimaeM56LDDe2EOFNRDxZUmgWWErGRl1DtwYOiSoPQI9R7jtfprmchgu7Ydvm1Fzzzydp7ezXbms+3SDgGmCUB8ZwuXr1hO353tlgsgl60xgwan7f73bDZ8AFvBWQlJmsEPigUBy6oNpRBDy46UIj8suiSjxSrdM1uvCkzGj4kw7iefD9sQqIdT214ZPgnSPd4skDUkn1ZNroSStM+wHzUbLwNn4wpe/z+igfSsIcLKqXcgGKjKKGWu6W2jCCsk32ZAlTMt7rsMmsd86geXoiHvXnUtsA8f5YtJPN3A1nYg1lWOScIiY2ihBq8NbIHfN4B5yqioVwV86lQ5WAer2yjPvZmsU9KmQcbcPrAVq77F7KwBwsq1RIEyNlIS6hlofkQEDN2FweixHE6wzTPAcy3uh7FEmFetpWfUBtQe87DZ9tZIJ+UMD/3hd/iubZkDaLU2u8Csq2SG0hsziVaM7govHwbwKLbj7Kil7xlxR+vmSjj37ISI97JhL/SIsT6EpDKdb7CX4o2QCFDRR+OXvhC/ZborI587afU2gMFlcLdAGIjza4lTLwE8xl6sdCot1qLW/ZkCL8UvYyol3FhLP5Sdxx9Zy29yjw0WOgtR+MuuDUwgdTaA1IvlxvR2chKqGX7Q547rTXO4EQLDFvK5UYQm8k9WOTLmvh64cMuSL1YUEndjSg2cHY1YOLrhQPY2osFlVRueLGZvASJ3zZg4msG0NqLBZVMbnixgUuowdvrvs2/I0BrLxRUErnhxQZeyGfOxNcMkLUXCiqJ3HBiAw1QDi6a1CoDZO35ncrwID0vNsAslHkTXydA1p7LsqDccGIDLZNfNiZejRdCouWlHpQbRmyAEqox8Xp8aAvUswUVJDeM2IjZtTHx2fBdvYcKkBt20YFQQi33GxOfDYK1Z6UekBtabCb8AGVj4rPjV87aswWVKDe02PAl1HJZZpL7nwfO2jNZVnj1By02vMg3Jj4vOGtPF1RHgtxQYsOVUI2Jzw/O2tMFFS839EYRZo1HY+KLgbX2VJa9lIoNW0I1Jr4o2FF7aqcyNyeYFLBMdm1MfBnQ1p4qqFi5+QRmVyPLaf7BeE0pTpplmUH6VGyYlzW9v+6fftNBW/tE6pn13MnLPagSqtxK1QYI1MrXV5DckBVOVHYdfGtSqwmk1j4pqCi5IWKTvqxp2Zh4UzhOpseThfWfUuYvuRKqWU5jEom1J8OWqdxgZzMhJVRj4s0isfY4yyZyg8WGiPyy4HvJGsjxdkAXVOkCy0u6hFpeNCbePLC1xwUVkZvY2ZASqjHx1eD4gnpjPZYbJDaohGpG4qsDsvbxTmVcTMXOBpVQjYmvEh/j6fG4oEK71U5Jdm1MfMWIR+3jgiqWm2hXWlxCNSPx1SOy9lGWjd1NJDbRy5oG36/7Z/0TcD+09lFBFcnN6UlUQjUm/qrwNv4XYKHchM4mFHmz280aqPB6uTyfhO5m5zIsoRoTf5UIrf3LycbO6dG97WYk/opxNnhzubNxtGxM/JXj4/a9nctS/3OxQUEcf//x38bEXw/+1yhNPvwfsrqUoBskFIcAAAAASUVORK5CYII=)

---
<br>
I hope this can be helpful and now we can say godbye to ~~git problems~~
